# School DIM -- space viewer

A school project realized in 4 hours
The goal was to develop a solar system (without real values, just the speed calculated according to the distance from the sun)
At least one interaction needed (click add one new planet, zoom in/out)


## Getting started

First, clone the project
Then, move to the directory in command line, and launch :

```
python space.py
```

You can also launch this script using an IDE like Anaconda.

