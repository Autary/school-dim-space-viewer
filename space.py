# -*- coding: utf-8 -*-
"""
Created on Tue Oct 19 18:58:58 2021

@author: nicolas wadoux
With Spyder Anaconda3
"""

import sys, math, pygame
import random as random

pygame.init()

#constantes

screenW, screenH = 1024,768
spaceW, spaceH = 256, 192

X, Y = 0,1
ZOOMSTEP = 10

# Ces var servent à se placer dans un espace carré en prenant le max de place possible sur l'écran, 
#  selon la distance des planetes en mettant la plus grande au bord maximum (comme les orbites sont ciruclaires) 
#  qui sont dans le tableau planets pour le zoom initial
maxSpaceSize = min(spaceW, spaceH)/2
maxScreenSize = min(screenW, screenH)/2

screen = pygame.display.set_mode((screenW, screenH))

# background for image
spaceBg = pygame.transform.scale(pygame.image.load("space.gif"), (screenW, screenH))

font = pygame.font.SysFont('Arial', 12)

class Planet():
    def __init__(self, name, size, color, d, childs = None, coords = (0,0), image = None):
        self.name = name        # nom de la planete
        self.size = size/2      # arbitraire car compliqué si on ajoute de grosses planetes comme jupiter
        self.color = color      # couleur du display
        self.d = d              # distance au parent (millions de km)
        self.angle = 0          # angle de rotation
        self.childs = childs    # planetes gravitant autour
        self.coords = coords    # coordonnées sur l'écran de la planete
        self.image = pygame.image.load(image).convert_alpha() if image != None else None
        
    # display planet on screen
    def display(self, parentX, parentY):
        # calc distance and coords on real screen
        planetFromParent = maxSpaceSize * self.d / dMax 
        planetFromParentOnScreen = planetFromParent / maxSpaceSize * maxScreenSize
        
        # -1 sens antihoraire
        planetX = parentX + math.cos(self.angle) * -1 * planetFromParentOnScreen
        planetY = parentY + math.sin(self.angle) * planetFromParentOnScreen
            
        self.coords = (planetX, planetY)       
        
        if(self.d > 0):
            pygame.draw.circle(screen,  (150,150,150), (parentX, parentY), planetFromParentOnScreen , width=1)
            self.angle+=0.5/self.d*2
            if self.angle >= 360:
                self.angle = 0
        
        
        planetSize = self.size / maxSpaceSize * maxScreenSize
        
        if self.image != None:
            image = pygame.transform.scale(self.image,  (planetSize*2, planetSize*2))
            rectImg = image.get_rect()
            rectImg.center = self.coords
            screen.blit(image,rectImg)
        else:
            pygame.draw.circle(screen,  self.color, self.coords, planetSize)
            
        # display text at middle of the planet
        planetTxt = font.render(self.name, True, (255,255,255))
        txtX, txtY = self.coords[X]-planetTxt.get_width()/2, self.coords[Y]-planetTxt.get_height()/2
        screen.blit(planetTxt, (txtX, txtY))
        
    # recursive function to display a planet and its planets that orbit around it
    def moveChilds(self, parentCoords = None):
        if parentCoords == None: 
            x,y = self.coords
        else:
            x,y = parentCoords
            
        self.display(x, y)
        
        if self.childs != None:
            for planet in self.childs:
                planet.moveChilds(self.coords)
        
#### flags
# click add new orbit
newOrbit = None

# planets that we want directly on the screen without zoom
planets = [
    Planet("Mercury", 4, (80, 78, 81), 57.91),
    Planet("Venus", 7, (165, 124, 27), 108.2),
    Planet("Earth", 10, (67, 128, 127), 149.6, [
        Planet("Moon", 2, (200, 200, 200), 24) #distance arbitraire
    ], (0,0), "earth.png"),
    Planet("Mars", 7, (161, 37, 27), 227.9)
]

# Determine la distance max des planetes que je veux dans le zoom par default, on a donc dès le debut mars tout au bord de l'écran
dMax = 0
for planet in planets:
    dMax = planet.d if planet.d > dMax else dMax

# Ajout des planetes en plus, il faudra dézoomer pour les voir
# les tailles sont arbitraires et multipliés à cause du zoom, sinon on ne voit que de petits points
planets.extend([
    Planet("Jupiter", 50, (216, 202, 157), 778.5, [
        Planet("Io", 11, (217, 208, 110), 145),
        Planet("Europa", 5, (239, 226, 221), 170),
        Planet("Ganymede", 15, (123, 114, 100), 80),
        Planet("Callisto", 11, (61, 52, 65), 115),
    ]),
    Planet("Saturn", 65, (123, 120, 105), 1434, [
        Planet("Titan", 30, (204, 156, 62), 250)
    ]),
    Planet("Uranus", 80, (187, 225, 228), 2871),
    Planet("Neptune", 80, (33, 35, 84), 4495)
])

# Create sun, center of this solar system
Sun = Planet("Sun", 30, (252,212,64), 0, planets, (spaceW/(2*spaceW)*screenW, spaceH/(2*spaceH)*screenH), "sun.png")

play = True
clock = pygame.time.Clock()

while play:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            play = False
        if event.type == pygame.MOUSEMOTION:
            pass
        if event.type == pygame.MOUSEBUTTONUP:
            # left click (add new orbit)
            if event.button == 1:
                newOrbit = pygame.mouse.get_pos()
                
            # wheel up (zoom in)
            if event.button == 4:
                maxScreenSize += ZOOMSTEP
                    
            # wheel down (zoom out)
            if event.button == 5:
                maxScreenSize -= ZOOMSTEP
                if maxScreenSize < 1:
                    maxScreenSize = 1
                
        if event.type == pygame.KEYUP:
            print(event.key, event.unicode, event.scancode)
            if event.key == pygame.K_ESCAPE:
                play = False

    screen.fill((0,0,0))
    screen.blit(spaceBg, (0, 0))
    
    if newOrbit != None:
        newSize = random.randint(2,20)
        # distance sur l'écran
        newD = math.sqrt(math.pow(newOrbit[X]-Sun.coords[X],2) + math.pow(newOrbit[Y]-Sun.coords[Y],2))
        # prevent planet spawn on sun by margin
        if newD > ((Sun.size + newSize) / maxSpaceSize * maxScreenSize):
            # distance sur le plan fictif
            newD = maxSpaceSize * newD / maxScreenSize
            # distance sur le plan réel (espace, dont j'ai besoin pour la création de l'objet) 
            newD = newD*dMax/maxSpaceSize
            
            newColor = (random.randint(0,255), random.randint(0,255), random.randint(0,255))
            Sun.childs.append(Planet("Alpha L"+str(random.randint(1000,9999))+"D", newSize, newColor, newD))
        
        newOrbit = None
    
    Sun.moveChilds();

    clock.tick(60)
    pygame.display.flip()